# DeFinity Legend

## Introduction

Enter the fantastic world of DeFinity Legend, ruled by 5 elements. Battle monsters in campaigns, raids or battle other players in arenas and Nation War to earn rewards, which can be traded for real money!

With DeFi and NFT spearheading the space, Definity Legends is a blockchain based Role-Playing Game which combines the elements of DeFi, NFT & Play2Earn in one.&#x20;

The first to combine potential NFT art listed within DLD marketplace to come to live within the DLD world!

Players collect NFT Characters, Guardians, Equipment and Land, to earn items, rewards and progress which can be sold to other players for BITTO POTION!

![BITTO Potion Earned From Game Play](.gitbook/assets/Untitled.png)

### Design Goals

* Provide players with an enjoyable, combat-oriented gameplay experience, while making real money as a reward for playing the game.
* Create a gripping world full of interesting encounters and content.
* Enable players to socialize and work together in achieving their goals.
* Design a fair and compelling progression throughout the game, by means of armors, guardians, heroes as they strive to build the best team of heroes they can.





