# Heroes

Heores are avatars who have certain attributes and skills that enable them to partake in combat.

Heroes are the main player character and their teammates. These are the characters that players equip and deploy in combat. The combat party consists of the main hero, and up to 5 other heroes.\
****

**Each Hero comes with the following attributes:**

* Health: The total health pool of the Hero. Hero becomes incapacitated and unavailable for combat when it reaches 0.
* Attack: Total attack power of the Hero.
* Defense: Total defensive power of the Hero.
* Elemental Affinity: Attunement of a hero to a particular element. Each hero can attune to a single element, except the Main Player Hero who is attuned to all elements.

Health, Attack and Defense can be increased by levelling up heroes or acquiring better equipment. Elemental Affinity can only be increased by fusing existing heroes to create improved versions.

In addition to simple attack and defense, each hero has 4 additional skills that define their combat output. These skills can also be levelled up using resources.

### ****

### ****
