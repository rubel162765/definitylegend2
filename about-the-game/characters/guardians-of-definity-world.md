# Guardians Of #DeFinity World

Guardians are elemental beings that can be summoned into battle by players. Once summoned, they automatically battle alongside the player until they are defeated or the battle ends.

Guardians are similar to heroes in that they also have HP, Attack, Defense and Skills. But unlike heroes, they cannot equip items, cannot be fielded instead of heroes and have only single skills.

## Classifications

There are 7 Tiers of Guardians:

1. Common: 50% Elemental Affinity
2. Uncommon: 55% Elemental Affinity
3. Rare: 60% Elemental Affinity
4. Very Rare: 65% Elemental Affinity
5. Epic: 70% Elemental Affinity
6. Legendary: 80% Elemental Affinity
7. Mythic: 84% Elemental Affinity (To be added at a later date)

## **Elements**

Guardians are divided into 5 different elements.

![Guardian Of Fire - Demon](../../.gitbook/assets/b678a27d5a92d1efd4c0d27168edd05f.0.jpg)

**Fire** - Demons

Demons, the elementals of fire, then taught the sages to manipulate the fiery power to melt and mold metal. The sages of Ignis became great smiths, learning to create metal tools, weapons, and armors.

****

![Guardian Of Earth - Golem](../../.gitbook/assets/8c9686d152a41468ce3043011b6b0145.0.jpg)

**Earth** - Golem

Golems, the elementals of earth, taught the sages how to manage different types of stone and earth. The sages of Petra became great builders, learning to create walls, houses, and fortifications.

![Guardian Of Wind - Sylph](<../../.gitbook/assets/final\_Wind Sylph\_fix (1).jpg>)

**Wind** - Sylph

Sylphs, the elementals of air, taught the sages to channel the subtle energies of air, which inspired creativity and imagination. The sages of Ventum became great inventors, conceiving writing and music



![Guardian Of Water - Dragon](<../../.gitbook/assets/208074ec8a520d67b496bb3ff3789fd3.0 (1).jpg>)

**Water** - Dragon

Water Dragons, the elementals of water, taught the sages the inner (deep) and outer (shallow) manipulation of the water. The sages of Aqua became great healers and invented agriculture.

****

****

****\
****

![Guardian Of Aether - The Strongest, Archangel ](<../../.gitbook/assets/Archangel (3) (1).jpg>)



**Aether -** Archangel **(Archangel** is the only **Guardian** with a healing buff)

The mercenaries, who named their army of "Army of the Chosen", joining large numbers of their fellows for the first time, pooled their powers and prior knowledge of the other elements, creating a powerful ritual that allowed some of them to summon, for the first time in history, the power of the planet's supreme guardians, the archangels.&#x20;

The Battle of the Cosmic Plains didn't need to be won. The mythical guardians' first displays of power were enough for the enemy armies to retreat, running for their lives.&#x20;

The stories say that the symbols painted on the ground by the Aetherians to summon the first Archangels were eternally imprinted on the rocks of the Hills, and, to this day, any inhabitants of Petra and Aqua would rather die than set foot there again
