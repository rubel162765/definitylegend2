# Guilds

Guilds can be founded through deeds that can be purchased by the players. Once founded, other players can be invited into the guilds to form the ranks. Guilds can wage wars against each other to conquer the territories of Definity Legends.

Guilds can vary in size depending on the acquired deed. There are 4 guild sizes available:

* Company: 20 Players
* Brigade: 30 Players
* Legion: 40 Players
* Corps: 50 Players

![Divided Territories Of Definity World ](<../.gitbook/assets/Territory War.jpg>)

### Nation War

Guilds can bid for war using **DLD tokens**. When a guild joins the war against other guilds for the region, its members can battle the opposing guild members in asynchronous PvP matches. Defeating opponents in battles grants points according to their ranking in their respective guilds. When the battle is over, the winning guild takes the land and hold it for the remainder of the month.

80% of the tokens from the highest bidder will be used as daily payments for the Territories held. (???)

