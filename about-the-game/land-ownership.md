# Land Ownership

The first genesis 2376 lands will be sold.&#x20;

![As you increase your land points, you increase your % earnings.](../.gitbook/assets/Zone\_Land.png)

&#x20;

### Land profit sharing

67% of the profits made from advertising Billboards will be shared with Land Owners.

Lands that are within Zone 1 & 2 to the Center of the Advertising area will get a 30% & 20% share of the revenue. Zone 3 & 4 will share 10% and 7% ( from users advertising on a billboard in the centre )

Lands will be used to earn resources, materials, blueprints and passive income for in-game gold currency which can be used to upgrade the land plots to increase land points.

Higher land points will increase the % earned from the advertising area % probability of DLD token drop.&#x20;

Zone 1 - will have 360 lands

Zone 2 - will have 504 lands

Zone 3 - will have 648 lands

Zone 4 - will have 864 lands



### Advertisement Tracking For Advertisers (CPC)

Users can claim 3% of the billboard revenue by logging in daily by clicking on the billboard.&#x20;

This allows the billboard advertiser to gauge daily views and awareness based on a CPC indicator.

## **What are the dimensions of each plot of Land?** <a href="#8876" id="8876"></a>

Every plot of land consists of 36 blocks (6x6)



**Resource Gathering and Crafting**\




