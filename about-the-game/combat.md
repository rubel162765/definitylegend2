# Combat

Combat in Definity Legends is driven by turn-based Strategy/RPG mechanics.  Players pick a team of heroes, including their main hero, and battle against a similarly fielded force of opponents.&#x20;

Actions that can be taken in during combat revolve around HP, Attack and Defense stats. Higher the attack value, the more damage each character will inflict, especially on opponents that have lower defense attributes than their attack attribute.

Characters also earn skills as they level up, that can be useful during combat. While some skills are passive and always in use, most skills can be used only after the character has filled up their skill bar in combat. The only exception to this rule is the Basic Attack, which is always available.&#x20;

In each turn, players can pick to use one skill per character, from the skills available to that character. Once all characters have taken their turn, the opponents execute their own skills. Unless there are specific limiters in place, such as time or turn limit, this continues until one side is completely defeated.

In addition to their characters, players also get **guardians** that they can deploy during combat. Just like characters, guardians also have skills, in addition to HP, Attack and Defense attributes. But unlike party characters, they cannot equip items, cannot be fielded as party characters and each can have only one skill. Deployed guardians trigger their skills automatically when their skill bar is full.
