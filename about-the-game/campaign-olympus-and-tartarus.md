# Campaign, Olympus and Tartarus

There are 3 main events where the players can engage in PvE battles;

* Campaign
* Test of Olympus
* Bowels of Tartarus

**Campaign** is a linear progression of PvE battles, fought against common, rare, legendary and epic monsters. Each campaign chapter consists of 9 regular fights and a boss fight, in which higher tier monsters appear. Players can win battles and complete chapters to earn rewards.

**Test of Olympus** is a battle arena, where players can fight previously defeated campaign monsters, to earn tickets that enable them to participate in the Bowels of Tartarus.

The most difficult PvE fights in the game are fought in the **Bowels of Tartarus**, where the players engage in battles against most powerful bosses in Definitely Legend. Winning these battles grant the user some of the best rewards in the game.

