# Itemization

Players can gather various types of items in Definity Legends. Main Categories of items are;

* Weapons
* Armor
* Artifacts
* Consumables

Certain items such as Weapons and Armor are element and hero specific and can only be equipped by hero which have the matching elemental affinity.

Each item has qualities that are divided into tiers. There are 6 tiers in Definity Legends;

* Common (Non-NFT, Non-Tradable)
* Uncommon : **15%** drop rate from chests.
* Rare: **12%** drop rate from chests.
* Very Rare: **9%** drop rate from chests.
* Epic: **5%** drop rate from chests.
* Legendary: **3%** drop rate from chests.

Common items are non-NFT items that players start the game with and can be rewarded in the beginner stages of the game. They cannot be traded in the marketplace.

## Weapons

Weapons are equipped by heroes to increase their Attack stat. They are split into tiers to signify their strength.

## Armor

Armors effect the Defense stat of heroes. They have tiers just like weapons. Armors can additional grant bonus stats, and even skills when the hero equips them as a set.

Available types of armor in the game are;

* Shield
* Helmet
* Gauntlet
* Leggings
* Breastplate

Out of all the armor types, shield is the only optional slot, which can equip a secondary weapon instead of a shield, to increase attack rather than defense.

## Consumables

Consumables are frequently used in Definity Legends to progress through the game in form up upgrades and fighting more battles.&#x20;

Common consumables in the game are:

* **Bitto Potion:** Key ingredient for many upgrades, fusing and the main currency in the DeFI marketplace.
* **Stamina Potion:** Can be used to refill the stamina of hero(es).
* **Health Potion:** Can be used to heal the hero(es).
* **Metals:** A staple of upgrading armors and weapons. An NFT resource that is earned throughout the game.
* **Exp Books:** Used for upgrading Guardians.

****

