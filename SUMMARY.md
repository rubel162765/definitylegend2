# Table of contents

* [DeFinity Legend](README.md)

## About the Game

* [Core Gameplay](about-the-game/core-gameplay.md)
* [Heroes](about-the-game/characters/README.md)
  * [Guardians Of #DeFinity World](about-the-game/characters/guardians-of-definity-world.md)
* [Itemization](about-the-game/equipment.md)
* [Campaign, Olympus and Tartarus](about-the-game/campaign-olympus-and-tartarus.md)
* [Combat](about-the-game/combat.md)
* [Guilds](about-the-game/guilds.md)
* [Land Ownership](about-the-game/land-ownership.md)
* [Earning Assets](about-the-game/earning-assets.md)

***

* [Economics](economics.md)
